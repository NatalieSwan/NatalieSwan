# Hi there! 👋

## I'm a Data Analyst

<p align="left">
  <img width="200" height="200" src="https://i.giphy.com/media/v1.Y2lkPTc5MGI3NjExaHh1MndzczhmaTVqcjNocTBvaDMwc20zaXFvNjI3Z2JtNTM2NWR1MSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/92cu6TfCZDVRBkmmDu/giphy.gif">
</p>


My dashboards <p id="badges" align="left">
<a href="https://public.tableau.com/app/profile/natalie.swan3088/vizzes">
    <img src="https://img.shields.io/badge/Tableau-white?logo=Tableau&logoColor=blue&style=for-the-badge" alt="Tableau Badge"/>
</p>  



## My opensource projects

Project | Discribe | Stack |
--------|----------|-------|
[E-Commerce Product Analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis) - Project for E-commerce with Python. | Includes: [Product analysis, EDA](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads), [Cohort analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads), [RFM analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_segmentation.ipynb?ref_type=heads) | ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) Pandas, Numpy, Seaborn, Matplotlib |
[A/B testing a new payment mechanics](https://gitlab.com/python-data-analytics-projects/a-b-testing-new-payment-mechanics) | Project for e-learning with Python | ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54), Pandas, Numpy, Pingouin, SciPy.stats, Matplotlib, Seaborn, requests, urllib.parse - for loading datasets via API, EDA, methods of statistical analysis: Chi-square (Pearson's criterion), Student’s t-test |
[Big query with SQL](https://gitlab.com/sql-projects3/big-query-sql) | Complex query to calculate metrics based on data from 3 datasets. | ![SQL](https://img.shields.io/badge/-SQL-00A4EF?style=for-the-badge&logo=SQL) subqueries, join, ClickHouse, Python/pandahouse |
[Window functions SQL](https://gitlab.com/sql-projects3/window-functions-sql) | Queries using window functions | ![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-FFF?style=for-the-badge&logo=PostgreSQL) Window functions, Redash | 
[Dashboard in Redash](https://gitlab.com/data-visualization8525117/dashboard-in-redash) | Dashboard created on the Redash platform using SQL. Provides an overview of the main sales metrics of e-Commerce. | ![SQL](https://img.shields.io/badge/-SQL-00A4EF?style=for-the-badge&logo=SQL) subqueries, filters, grouping, Redash, ClickHouse |
[Dashboards in Tableau](https://gitlab.com/data-visualization8525117/dashboards-in-tableau) | My data visualization projects in Tableau: *  Employee Satisfaction, *  Cohort analysis, *  What is the salary for an analyst in Russia?,*  Profits overview by regions, states, categories | ![Tableau](https://img.shields.io/badge/Tableau-white?logo=Tableau&logoColor=blue&style=for-the-badge) Python |

## About Me:

Before I completed a Data Analyst course and got the new profession, I had the following background:
* 2 years as a marketing analyst, 
* 5 years as a lead project manager. 

During five years as a project manager, among other duties, I monitored product economic metrics and took steps to improve them, set product prices, and calculated ROI on new product lines.   
 
🏆 At this point, I started working as a Data Analyst/BI Analyst for the Interbank Processing Center (IPC), Bishkek, Kyrgystan. Wish me good luck, please :)

### I believe that the right decisions leading to business success stand behind the good analyzing data. Here are just two examples from my career: 

1. One day, the company that owned the main chain of photo labs in the city suddenly sold it and exited the business without a loss. A few weeks after that, the market for photo services collapsed. And I am proud that my market research report and correctly made forecasts pushed the company's management to make the right move. 
2. Because of my love of analyzing, I didn't raise prices immediately every time I saw a product's profitability drop, as was my company's custom. First, I would examine the data and find the cause of the problem. Then, depending on the results of my analysis, we would either change one of the ingredients, negotiate with the supplier to freeze the price of raw materials, or take other measures that allowed us to improve profitability to the same level without raising the product's price. 

## My other projects

🔎 **Research** of consumers' perception of children's trademark image and use of its results in developing of a rebranding strategy.

🏆 Based on the research, I developed a concept for a children's brand and a rebranding strategy. 

🔎 **Analysis** of the pharmaceutical labor market in Tomsk.

🏆 I made conclusions about the current market situation and proposed to the management of the Department of Pharmacy Faculty of the Medical University several activities to improve the position of their graduates. In particular, I proposed to teach the students the basics of commercial activity, which caused an explosion of laughter from the audience.  

🔎 At the request of a pharmaceutical distributor, I conducted a complete **analysis** of their competitors (product range, price analysis, promotion methods).

🔎 Photo services market **analysis**

🏆 The company exited the photo services business without losses, having managed to sell a chain of photo labs before the market collapsed, thanks to my market **research** report and correctly made forecasts.


## ⚒️ Technical Stack


![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white)
![SciPy](https://img.shields.io/badge/SciPy-%230C55A5.svg?style=for-the-badge&logo=scipy&logoColor=%white)
![Matplotlib](https://img.shields.io/badge/Matplotlib-%23ffffff.svg?style=for-the-badge&logo=Matplotlib&logoColor=black)
![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white)
![Jupyter](https://img.shields.io/badge/-Jupyter_Notebook-FFF?style=for-the-badge&logo=Jupyter)
![SQL](https://img.shields.io/badge/-SQL-00A4EF?style=for-the-badge&logo=SQL)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-FFF?style=for-the-badge&logo=PostgreSQL)
![Clickhouse](https://img.shields.io/badge/-Clickhouse-FFF?style=for-the-badge&logo=Clickhouse)
![Redash](https://img.shields.io/badge/-Redash-E44D26?style=for-the-badge&logo=Redash)
![EXCEL](https://img.shields.io/badge/-EXCEL-FF?style=for-the-badge&logo=EXCEL)
![Google_Sheets](https://img.shields.io/badge/-Google_Sheets-FFF?style=for-the-badge&logo=GoogleSheets)
![GIT](https://img.shields.io/badge/-GIT-FFF?style=for-the-badge&logo=GIT)
![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=orange)
![Airflow](https://img.shields.io/badge/-Airflow-77DDE7?style=for-the-badge&logo=AIRFLOW)
![Tableau](https://img.shields.io/badge/Tableau-white?logo=Tableau&logoColor=blue&style=for-the-badge)


## 🔎Key competencies

*   Unit economics
*   Economic analysis
*   Statistical analysis
*   A/B tests
*   Marketing analysis


## 🎓 Education

🎓 Completed a course and received a passing grade in the **Data analyst** course of [Karpov.courses.](https://karpov.courses/) 

[My certificate](https://gitlab.com/NatalieSwan/NatalieSwan/-/blob/main/DataAnalystEng.pdf)

Subjects studied:

    *    Statistics
    *    A/B tests
    *    Python
    *    SQL
    *    Tableau
    *    Git
    *    Airflow
    *    Probability theory 
    *    Product analytics  

## 🎓 High Education  
🎓 **Marketing in commercial activity** - [Tomsk State University](https://iem.tsu.ru/en/)

Among the subjects studied were:

    *    Statistics 
    *    Economic analysis 
    *    Marketing research   
    *    Business economics   
    *    Economic theory    
    *    Finance, monetary circulation, and credit. 

🏆 Graduated with honors.


🎓 **Pharmaceutical specialist** - [Siberian State Medical University](https://abiturient.ssmu.ru/en/)  

Among the subjects studied were:

    *    Higher mathematics, including probability theory  
    *    Organization and economics of pharmacy.

## My contacts

<div id="badges" align="left">
<a href="https://t.me/Natalie_L_Swan">
    <img src="https://img.shields.io/badge/Telegram-blue?style=for-the-badge&logo=Telegram&logoColor=white" alt="Telegram Badge"/>
  </a>
  <a href="https://www.linkedin.com/in/natalie-swan-269158337/">
    <img src="https://img.shields.io/badge/LinkedIn-blue?style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/> 
  </a>    
</div>
<p align='center'>
   📫 <a href='mailto:tasha.l.swan@gmail.com'>tasha.l.swan@gmail.com</a>
</p>
