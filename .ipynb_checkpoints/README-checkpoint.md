# Hi, I'm Natalie 👋

Data Analyst. 

<p align='center'>
   <a href="https://www.linkedin.com/in/natalie-l-swan/">
       <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white"/>
   </a>
   <a href="https://t.me/Natalie_L_Swan">
       <img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white"/>
   </a>
<p align='center'>
   📫 How to reach me: <a href='mailto:tasha.l.swan@gmail.com'>tasha.l.swan@gmail.com</a>
</p>


## 🛠 Technical Stack

*   Python (Pandas, Seaborn, Matplotlib, NumPy, SciPy)
*   PostgreSQL/Redash, ClickHouse  
*   Tableau
*   Excel
*   GitHub, GitLab, Airflow

### Key competencies

*   Unit economics
*   Economic analysis
*   Statistical analysis
*   A/B tests
*   Marketing analysis

### My opensource projects

*   [E-Commerce Product Analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis) - Project for E-commerce with Python. Includs:
    *    [Product analysis, EDA](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/Project_e-commerce_by_Natalie_Lebedeva(Swan).ipynb?ref_type=heads)
    *    [Cohort analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/User_cohort_analysis.ipynb?ref_type=heads)
    *    [RFM analysis](https://gitlab.com/python-data-analytics-projects/e-commerce-product-analysis/-/blob/main/rfm_segmentation.ipynb?ref_type=heads)
*   [Dashboard in Redash](https://gitlab.com/data-visualization8525117/dashboard-in-redash) - Dashboard created on the Redash platform using SQL. Provides an overview of the main sales metrics of the e-Commerce.
*   [Dashboards in Tableau](https://gitlab.com/data-visualization8525117/dashboards-in-tableau) - My data visualization projects in Tableau


Completed a course and received a passing grade in the Data analyst course of Karpov.courses.

[My certificate](DataAnalystEng)

Currently I'm looking for a job as a data analyst.


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/NatalieSwan/NatalieSwan/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)




